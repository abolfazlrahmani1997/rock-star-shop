<?php

namespace Repositories;


use App\Models\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;


class ProductRepositoryTest extends TestCase
{
    /**
     * @var ProductRepositoryInterface|mixed
     */
    private ProductRepositoryInterface $productRepository;

    /**
     * initialize
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->productRepository = $this->app->make(ProductRepositoryInterface::class);
    }

    /**
     * Test
     * @return void
     */
    public function testGetProduct()
    {
        $product = $this->getProductInDb();
        $this->assertInstanceOf(Product::class, $product);
        $productInDb = $this->productRepository->getProduct(id: $product->id);
        $this->assertInstanceOf(Product::class, $productInDb);
        $this->assertEquals($product->id, $productInDb->id);
    }

    /**
     * Test Get All Data From Repository
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function testGetAllProducts()
    {
        $collection = $this->getProductInDb(count: 3);
        $this->assertInstanceOf(Collection::class, $collection);
        $collection_inDb = $this->productRepository->getAllProducts();
        $this->assertInstanceOf(Collection::class, $collection_inDb);
        foreach ($collection_inDb as $product) {
            $this->assertInstanceOf(Product::class, $product);
        }
    }

    /**
     * Test Create Product
     * @return void
     */
    public function testCreateProduct()
    {
        $product_data = $this->getProductData();
        $product = $this->productRepository->createProduct(data: $product_data);
        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals($product->title, $product_data['title']);
    }

    public function testUpdateProduct()
    {
        $product_data = $this->getProductData();
        $product_temp = $this->getProductInDb();
        $result = $this->productRepository->updateProduct(product: $product_temp, data: $product_data);
        $this->assertEquals($result->title,$product_data['title']);
    }

    /**
     * Delete Product
     * @return void
     */
    public function testDeleteProduct()
    {

        $product_temp = $this->getProductInDb();
        $result = $this->productRepository->deleteProduct(product: $product_temp);
        $this->assertEquals(true,$result);
    }
    private function generateProductFactory(): \Illuminate\Database\Eloquent\Factories\Factory
    {
        return Product::factory();
    }

    /**
     * Generate data Array
     * @return array
     */
    private function getProductData(): array
    {
        return $this->generateProductFactory()->make()->toArray();
    }

    /**
     * Generate data Array
     * @return Product
     */
    private function getProductInDb(int $count = null): Product|Collection
    {
        return $this->generateProductFactory()->when(isset($count), function ($q) use ($count) {
            return $q->count($count);
        })->create();
    }

}
