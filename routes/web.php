<?php

use App\Discount\Strategies\DiscountPattern;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    $order = \App\Models\Order::first();
//    $discount_r = \App\Models\Discount::first();
//
//    $discount = new DiscountPattern();
//    $discount->OrderDiscount(discount: $discount_r,order: $order);
//});
