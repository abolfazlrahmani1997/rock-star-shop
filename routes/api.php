<?php

use App\Http\Controllers\AttributeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SpecificationController;
use App\Http\Controllers\AttributeProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


    Route::prefix('product')->name('product')->group(function () {
        Route::get('/get', [ProductController::class, 'getProduct'])->name('get');
        Route::get('/getAll', [ProductController::class, 'getAllProduct'])->name('getAll');
        Route::post('/create', [ProductController::class, 'createProduct'])->name('create');
        Route::put('/update', [ProductController::class, 'updateProduct'])->name('update');
    });

    Route::prefix('order')->name('order')->group(function () {
        Route::get('/get', [OrderController::class, 'getOrder'])->name('get');
        Route::get('/getAll', [OrderController::class, 'getAllOrder'])->name('getAll');
        Route::post('/create', [OrderController::class, 'createOrder'])->name('create');
        Route::post('/add', [OrderController::class, 'addOrderItem'])->name('create');
        Route::put('/update', [OrderController::class, 'updateOrder'])->name('update');
    });

    Route::prefix('attribute')->name('attribute')->group(function () {
        Route::get('/get', [AttributeController::class, 'getAttribute'])->name('get');
        Route::get('/getAll', [AttributeController::class, 'getAllAttribute'])->name('getAll');
        Route::post('/create', [AttributeController::class, 'createAttribute'])->name('create');
        Route::put('/update', [AttributeController::class, 'updateAttribute'])->name('update');
        Route::delete('/delete', [AttributeController::class, 'deleteAttribute'])->name('delete');
    });

    Route::prefix('attribute-product')->name('attribute-product')->group(function () {
        Route::get('/get', [AttributeProductController::class, 'getAttributeProduct'])->name('get');
        Route::get('/getAll', [AttributeProductController::class, 'getAllAttributeProduct'])->name('getAll');
        Route::post('/create', [AttributeProductController::class, 'createAttributeProduct'])->name('create');
        Route::put('/update', [AttributeProductController::class, 'updateAttributeProduct'])->name('update');
        Route::delete('/delete', [AttributeProductController::class, 'deleteAttributeProduct'])->name('delete');
    });
