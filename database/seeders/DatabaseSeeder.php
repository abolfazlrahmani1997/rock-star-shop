<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Discount;
use App\Models\Order;
use App\Models\Product;
use App\Models\Shop;
use App\Models\ShopProduct;
use App\Models\User;
use Database\Factories\ShopProductFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         User::factory()->create();
//         Product::factory()->create();
        Category::factory(3)->create();
        Brand::factory()->create();
//        Order::factory(3)->create();
//        Discount::factory()->create();

//         \App\Models\User::factory()->create([
//             'name' => 'Test User',
//             'email' => 'test@example.com',
//         ]);
        Shop::factory()->for(User::factory()->create(),'user')->hasAttached(Product::factory()->count(3)->create(),['price'=>'1000'],'products')->create();
    }
}
