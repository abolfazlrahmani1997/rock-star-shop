<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Factory\ApiRequest;

class RegisterRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|min:0',
            'lastname' => 'required|string|min:0',
            'mobile' => 'required|string|min:0|unique:users,mobile',
            'email' => 'required|string|min:0|unique:users,email',
            'password' => 'required|string|min:0',
        ];
    }

    /**
     * return Data
     * @return array
     */
    public function getData(): array
    {
        return [
            'name' => $this->input('name'),
            'lastname' => $this->input('lastname'),
            'mobile' => $this->input('mobile'),
            'email' => $this->input('email'),
            'password' => $this->input('password'),
        ];
    }


}
