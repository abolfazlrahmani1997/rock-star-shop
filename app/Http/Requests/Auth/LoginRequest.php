<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Factory\ApiRequest;

class LoginRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required|string|min:0|exists:users,email',
            'password' => 'required|string|min:0'
        ];
    }

    /**
     * return Data
     * @return array
     */
    public function getData(): array
    {
        return [
            'email' => $this->input('email'),
            'password' => $this->input('password'),
        ];
    }
}
