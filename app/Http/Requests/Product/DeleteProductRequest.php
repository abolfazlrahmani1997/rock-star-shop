<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\Factory\ApiRequest;

class DeleteProductRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        return [
            'product_id' => "required|integer|exists:products,id",
        ];
    }

    /**
     * return data
     * @return array
     */
    public function getData(): array
    {
        return [
            'product_id' => $this->input('product_id'),
        ];
    }

    public function getProductId(): int
    {
        return $this->getData()['product_id'];
    }
}
