<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\Factory\ApiRequest;

class CreateProductRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        return [
            'brand_id' => 'nullable|integer|min:1|exists:brands,id',
            'title' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'attribute' => 'required',
            'attribute.*' => 'required|array',
            'attribute.*.attribute_id' => 'required|integer|exists:attributes,id',
            'attribute.*.price' => 'required|integer|min:0',
            'attribute.*.attribute_value' => 'required|string',
        ];
    }

    /**
     * return data
     * @return array
     */
    public function getData(): array
    {

        return [

            'sub_category_id' => $this->input('sub_category_id'),
            'brand_id' => $this->input('brand_id'),
            'title' => $this->input('title'),
            'description' => $this->input('description'),
            'price' => $this->input('price'),
            'attribute' => $this->input('attribute'),
            'specification' => $this->input('specification'),
            'files' => $this->file('files'),
        ];
    }
}
