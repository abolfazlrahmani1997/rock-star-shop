<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\Factory\ApiRequest;
use Illuminate\Foundation\Http\FormRequest;

class GetAllProduct extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules():array
    {
        return [
            'title' => 'nullable|string|max:255',
        ];
    }

    /**
     * return Data
     * @return array
     */
    public function getData(): array
    {
        return [
            'title' => $this->input('title') ?? null,
        ];
    }

}
