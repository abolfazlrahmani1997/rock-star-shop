<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\Factory\ApiRequest;

class UpdateProductRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        return [
            'product_id' => "required|integer|exists:products,id",
            'title' => 'nullable|string',
            'description' => 'nullable|string',
            'price' => 'nullable|string|max:11|min:11',
            'brand_id' => 'nullable|integer|min:1|exists:brands,id',

        ];
    }

    /**
     * return data
     * @return array
     */
    public function getData(): array
    {
        return [
            'title' => $this->input('title'),
            'description' => $this->input('description'),
            'price' => $this->input('price'),
            'brand_id' => $this->input('brand_id'),
            'product_id' => $this->input('product_id'),
        ];
    }

    /**
     * Get ProductsId
     */
    public function getProductId(): int
    {
        return $this->getData()['product_id'];
    }
}
