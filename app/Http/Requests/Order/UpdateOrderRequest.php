<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\Factory\ApiRequest;

class UpdateOrderRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        return [
            'order_id' => 'required|numeric|min:0|exists:orders,id',
            'user_id' => 'nullable|integer|min:1|exists:users,id',
            'total_amount' => 'nullable|string',
            'status' => 'nullable|string',
        ];
    }

    /**
     * return data
     * @return array
     */
    public function getData(): array
    {
        return [
            'order_id' => $this->input('order_id'),
            'user_id' => $this->input('user_id'),
            'total_amount' => $this->input('total_amount'),
            'status' => $this->input('status'),
        ];
    }

    public function getOrderId()
    {
        return $this->getData()['order_id'];
    }
}
