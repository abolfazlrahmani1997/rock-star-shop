<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\Factory\ApiRequest;

class CreateOrderRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required|integer|min:1',
//            'total_amount' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
//            'status' => 'required|string',
        ];
    }

    /**
     * return data
     * @return array
     */
    public function getData(): array
    {
        return [
            'user_id' => $this->input('user_id'),
            'total_amount' => $this->input('total_amount'),
            'status' => $this->input('status'),
        ];
    }
}
