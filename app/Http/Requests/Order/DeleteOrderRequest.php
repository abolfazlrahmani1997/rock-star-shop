<?php

namespace App\Http\Requests\Order;

use App\Http\Requests\Factory\ApiRequest;

class DeleteOrderRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        return [
            'order_id'=>'nullable|numeric|min:0|exists:orders,id',
        ];
    }

    /**
     * return data
     * @return array
     */
    public function getData(): array
    {
        return [
            'order_id' => $this->input('order_id'),
        ];
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->getData()['order_id'];
    }
}
