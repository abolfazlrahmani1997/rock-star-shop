<?php

namespace App\Http\Requests\Factory;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

abstract class ApiRequest extends FormRequest
{
    /**
     * @param Validator $validator
     * @return void
     */
    protected function failedValidation(Validator $validator)
    {
        throw (new HttpResponseException(response()->json($this->formatData($validator))));
    }

    /**
     * format data
     * @param Validator $validator
     * @return array
     */
    private function formatData(Validator $validator): array
    {
        return [
          'data' => [],
          'message' => $validator->errors()->all()
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        return [

        ];
    }

    /**
     * @return array
     */
    public function message(): array
    {
        return [

        ];
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return [

        ];
    }
}
