<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Factory\ApiRequest;

class UpdateUserRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'string',
            'lastname' => 'string',
            'mobile' => 'string|max:11|min:11',
            'email' => '',
            'password' => '',
        ];
    }

    /**
     * return data
     * @return array
     */
    public function getData(): array
    {
        return [
            'name' => $this->input('name'),
            'lastname' => $this->input('lastname'),
            'mobile' => $this->input('mobile'),
            'email' => $this->input('email'),
            'password' => $this->input('password'),
        ];
    }

    /**
     * get user id
     * @return int
     */
    public function getUserId(): int
    {
        return $this->getData()['user_id'];
    }
}
