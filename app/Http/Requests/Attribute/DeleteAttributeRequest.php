<?php

namespace App\Http\Requests\Attribute;

use App\Http\Requests\Factory\ApiRequest;

class DeleteAttributeRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        return [
            'attribute_id' => 'required|numeric|min:0|exists:attributes,id',
        ];
    }

    /**
     * return data
     * @return array
     */
    public function getData(): array
    {
        return [
            'attribute_id' => $this->input('attribute_id'),
        ];
    }

    /**
     * @return int
     */
    public function getAttributeId(): int
    {
        return $this->getData()['attribute_id'];
    }
}
