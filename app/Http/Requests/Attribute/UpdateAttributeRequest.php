<?php

namespace App\Http\Requests\Attribute;

use App\Http\Requests\Factory\ApiRequest;

class UpdateAttributeRequest extends ApiRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'attribute_id' => 'required|numeric|min:0|exists:attributes,id',
            'title' => 'required|string|min:0',
            'price' => 'required|integer|min:0',
            'category_id' => 'required|integer|min:0|exists:categories,id'
        ];
    }

    /**
     * get All reuqest
     */
    public function getData(): array
    {
        return [
            'attribute_id' => $this->input('attribute_id'),
            'title' => $this->input('title'),
            'category_id' => $this->input('category_id')
        ];
    }

    /**
     * return Category_Id
     */
    public function getCategory(): int
    {
        return $this->getData()['category_id'];
    }

    /**
     * @return int
     */
    public function getAttributeId(): int
    {
        return $this->getData()['attribute_id'];
    }
}

