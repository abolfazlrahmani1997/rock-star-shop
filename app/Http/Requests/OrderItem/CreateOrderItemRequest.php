<?php

namespace App\Http\Requests\OrderItem;

use App\Http\Requests\Factory\ApiRequest;

class CreateOrderItemRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        return [
            'order_id'=>'nullable|integer|min:0|exists:orders,id',
//            'product_id'=>'required|integer|min:0',
            'attribute_id'=>'required|integer|min:0'
        ];
    }

    /**
     * return data
     * @return array
     */
    public function getData(): array
    {
        return [
            'product_id'=>$this->input('product_id'),
            'attribute_id'=>$this->input('attribute_id'),
            'order_id'=>$this->input('order_id')
        ];
    }
}
