<?php

namespace App\Http\Requests\AttributeProduct;

use App\Http\Requests\Factory\ApiRequest;

class DeleteAttributeProductRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules(): array
    {
        return [
            'attribute_product_id' => 'required|numeric|min:0|exists:attribute_product,id',
        ];
    }

    /**
     * return data
     * @return array
     */
    public function getData(): array
    {
        return [
            'attribute_product_id' => $this->input('attribute_product_id'),
        ];
    }

    /**
     * @return int
     */
    public function getAttributeProductId(): int
    {
        return $this->getData()['attribute_product_id'];
    }
}
