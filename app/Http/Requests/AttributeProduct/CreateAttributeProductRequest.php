<?php

namespace App\Http\Requests\AttributeProduct;

use App\Http\Requests\Factory\ApiRequest;

class CreateAttributeProductRequest extends ApiRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'attribute_id' => 'required|integer|min:0|exists:attributes,id',
            'product_id' => 'required|integer|min:0|exists:products,id',
            'attribute_value' => 'required|string|min:0',
        ];
    }

    /**
     * get All reuqest
     */
    public function getData(): array
    {
        return [
            'attribute_id' => $this->input('attribute_id'),
            'product_id' => $this->input('product_id'),
            'attribute_value' => $this->input('attribute_value'),
        ];
    }
}

