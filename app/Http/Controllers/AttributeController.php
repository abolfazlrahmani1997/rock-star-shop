<?php

namespace App\Http\Controllers;

use App\Http\Requests\Attribute\CreateAttributeRequest;
use App\Http\Requests\Attribute\UpdateAttributeRequest;
use App\Http\Requests\Attribute\DeleteAttributeRequest;
use App\Services\Interfaces\AttributeServiceInterface;
use Illuminate\Http\JsonResponse;

class AttributeController extends Controller
{
    /**
     * Define Attribute for Repository
     * @var AttributeServiceInterface
     */
    private AttributeServiceInterface $attribute_service;

    /**
     * Injection To Attribute
     * @param AttributeServiceInterface $attribute_service
     */
    public function __construct(AttributeServiceInterface $attribute_service)
    {
        $this->attribute_service = $attribute_service;
    }

    /**
     * @return JsonResponse
     */
    public function getAttribute(DeleteAttributeRequest $request): JsonResponse
    {
        return $this->attribute_service->getAttribute(id: $request->getAttributeId());
    }

    /**
     * @return JsonResponse
     */
    public function getAllAttribute(): JsonResponse
    {
        return $this->attribute_service->getAllAttributes(data:  null);
    }
    /**
     * Create Attribute
     */
    public function createAttribute(CreateAttributeRequest $request): JsonResponse
    {
        $data = $request->getData();
        return $this->attribute_service->createAttribute(data: $data);
    }

    /**
     * @return JsonResponse
     */
    public function updateAttribute(UpdateAttributeRequest $request): JsonResponse
    {
        $data = $request->getData();
        $attribute_id = $request->getAttributeId();
        return $this->attribute_service->updateAttribute(id: $attribute_id, data: $data);
    }

    /**
     * @return JsonResponse
     */
    public function deleteAttribute(DeleteAttributeRequest $request): JsonResponse
    {
        return $this->attribute_service->deleteAttribute(id: $request->getAttributeId());
    }
}
