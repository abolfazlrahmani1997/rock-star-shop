<?php

namespace App\Http\Controllers;

use App\Http\Requests\AttributeProduct\CreateAttributeProductRequest;
use App\Http\Requests\AttributeProduct\DeleteAttributeProductRequest;
use App\Http\Requests\AttributeProduct\UpdateAttributeProductRequest;
use App\Services\Interfaces\AttributeProductServiceInterface;
use Illuminate\Http\JsonResponse;

class AttributeProductController extends Controller
{
    /**
     * Define Attribute for Repository
     * @var AttributeProductServiceInterface
     */
    private AttributeProductServiceInterface $attribute_product_service;

    /**
     * Injection To Attribute
     * @param AttributeProductServiceInterface $attribute_product_service
     */
    public function __construct(AttributeProductServiceInterface $attribute_product_service)
    {
        $this->attribute_product_service = $attribute_product_service;
    }

    /**
     * @return JsonResponse
     */
    public function getAttributeProduct(DeleteAttributeProductRequest $request): JsonResponse
    {
        return $this->attribute_product_service->getAttributeProduct(id: $request->getAttributeProductId());
    }

    /**
     * @return JsonResponse
     */
    public function getAllAttributeProduct(): JsonResponse
    {
        return $this->attribute_product_service->getAllAttributeProducts(data:  null);
    }
    /**
     * Create Attribute
     */
    public function createAttributeProduct(CreateAttributeProductRequest $request): JsonResponse
    {
        $data = $request->getData();
        return $this->attribute_product_service->createAttributeProduct(data: $data);
    }

    /**
     * @return JsonResponse
     */
    public function updateAttributeProduct(UpdateAttributeProductRequest $request): JsonResponse
    {
        $data = $request->getData();
        $attribute_id = $request->getAttributeProductId();
        return $this->attribute_product_service->updateAttributeProduct(id: $attribute_id, data: $data);
    }

    /**
     * @return JsonResponse
     */
    public function deleteAttributeProduct(DeleteAttributeProductRequest $request): JsonResponse
    {
        return $this->attribute_product_service->deleteAttributeProduct(id: $request->getAttributeProductId());
    }
}
