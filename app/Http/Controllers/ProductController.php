<?php

namespace App\Http\Controllers;

use App\Helper\ResponseWrapper;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\DeleteProductRequest;
use App\Http\Requests\Product\GetAllProduct;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Resources\ProductResource;
use App\Services\Interfaces\ProductServiceInterface;
use Illuminate\Http\JsonResponse;

class ProductController extends Controller
{
    /**
     * Define Product Service
     * @var ProductServiceInterface
     */
    private ProductServiceInterface $product_service;

    /**
     * Define Response Wrapper
     * @var ResponseWrapper
     */
    private ResponseWrapper $response_wrapper;

    /**
     * Inject In Controller
     * @param ProductServiceInterface $product_service
     * @param ResponseWrapper $response_wrapper
     */
    public function __construct(ProductServiceInterface $product_service, ResponseWrapper $response_wrapper)
    {

        $this->product_service = $product_service;
        $this->response_wrapper = $response_wrapper;
    }

    /**
     *
     * @return void
     */
    public function getProduct(DeleteProductRequest $request): JsonResponse
    {
        return $this->product_service->getProduct(id: $request->getProductId());

    }

    /**
     * get All Products
     */
    public function getAllProduct(GetAllProduct $request): JsonResponse
    {
        $data = $request->getData();

        return $this->product_service->getAllProduct(data: $data);
    }


    /**
     * create Products
     */
    public function createProduct(CreateProductRequest $request): JsonResponse
    {
        $data = $request->getData();
        return $this->product_service->createProduct(data: $data);
    }

    /**
     * update Product
     * @param UpdateProductRequest $request
     * @return JsonResponse
     */
    public function updateProduct(UpdateProductRequest $request): JsonResponse
    {
        $data = $request->getData();
        $product_id = $request->getProductId();
        return $this->product_service->updateProduct(id: $product_id, data: $data);
    }

    /**
     * Delete Producted
     * @param DeleteProductRequest $request
     * @return JsonResponse
     */
    public function deleteProduct(DeleteProductRequest $request): JsonResponse
    {
        $product_id = $request->getData()['product_id'];
        return $this->product_service->deleteProduct(id: $product_id);
    }

}
