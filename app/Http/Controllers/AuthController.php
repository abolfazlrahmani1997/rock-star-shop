<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Services\Interfaces\AuthServiceInterface;
use App\Services\Interfaces\UserServiceInterface;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    /**
     * Service Display Interface
     */
   private AuthServiceInterface $auth_service;

   private UserServiceInterface $user_service;
    /**
     * Injection Service In Controller
     * @param AuthServiceInterface $category_service
     */
    public function __construct(AuthServiceInterface $auth_service,UserServiceInterface $user_service)
    {
        $this->auth_service = $auth_service;
        $this->user_service=$user_service;
    }

    /**
     * Register New User
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        $data = $request->getData();
        return $this->auth_service->register(data: $data);
    }

    /**
     * Register New User
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $data = $request->getData();
        return $this->auth_service->login(data: $data);
    }



}
