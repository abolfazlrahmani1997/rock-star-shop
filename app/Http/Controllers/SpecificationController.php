<?php

namespace App\Http\Controllers;

use App\Http\Requests\Specification\CreateSpecificationRequest;
use App\Http\Requests\Specification\DeleteSpecificationRequest;
use App\Http\Requests\Specification\UpdateSpecificationRequest;
use App\Services\Interfaces\SpecificationServiceInterface;
use Illuminate\Http\JsonResponse;

class SpecificationController extends Controller
{
    /**
     * define specification service
     * @var SpecificationServiceInterface
     */
    private SpecificationServiceInterface $specification_service;

    /**
     * inject service in controller
     * @param SpecificationServiceInterface $specification_service
     */
    public function __construct(SpecificationServiceInterface $specification_service)
    {
        $this->specification_service = $specification_service;
    }

    /**
     * Get one
     * @param DeleteSpecificationRequest $request
     * @return JsonResponse
     */
    public function getSpecification(DeleteSpecificationRequest $request): JsonResponse
    {
        return $this->specification_service->getSpecification(id: $request->getSpecificationId());
    }

    /**
     * Get all
     * @return JsonResponse
     */
    public function getAllSpecification(): JsonResponse
    {
        return $this->specification_service->getAllSpecification(data: null);
    }

    /**
     * Create
     * @param CreateSpecificationRequest $request
     * @return JsonResponse
     */
    public function createSpecification(CreateSpecificationRequest $request)
    {
        $data = $request->getData();
        return $this->specification_service->createSpecification(data: $data);
    }

    /**
     * Update
     * @param UpdateSpecificationRequest $request
     * @return JsonResponse
     */
    public function updateSpecification(UpdateSpecificationRequest $request)
    {
        $data = $request->getData();
        $order_id = $request->getSpecificationId();
        return $this->specification_service->updateSpecification(id: $order_id, data: $data);
    }

    /**
     * Delete
     * @param DeleteSpecificationRequest $request
     * @return JsonResponse
     */
    public function deleteSpecification(DeleteSpecificationRequest $request)
    {
        return $this->specification_service->deleteSpecification(id: $request->getSpecificationId());
    }

}
