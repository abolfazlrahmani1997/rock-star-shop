<?php

namespace App\Http\Controllers;

use App\Http\Requests\Order\CreateOrderRequest;
use App\Http\Requests\Order\CreateSpecificationRequest;
use App\Http\Requests\Order\GetOrderRequest;
use App\Http\Requests\Order\UpdateSpecificationRequest;
use App\Http\Requests\OrderItem\CreateOrderItemRequest;
use App\Services\Interfaces\OrderServiceInterface;
use Illuminate\Http\JsonResponse;

class OrderController extends Controller
{
    /**
     * define order service
     * @var OrderServiceInterface
     */
    private OrderServiceInterface $order_service;

    /**
     * inject service in controller
     * @param OrderServiceInterface $order_service
     */
    public function __construct(OrderServiceInterface $order_service)
    {
        $this->order_service = $order_service;
    }

    /**
     * get one order
     * @param UpdateSpecificationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrder(GetOrderRequest $request): JsonResponse
    {
        return $this->order_service->getOrder(id: $request->getOrderId());
    }

    /**
     * get all order
     * @return JsonResponse
     */
    public function getAllOrder(): JsonResponse
    {
        return $this->order_service->getAllOrder(data: null);
    }

    /**
     * @param CreateSpecificationRequest $request
     * @return JsonResponse
     */
    public function createOrder(CreateOrderRequest $request)
    {
        $data = $request->getData();
        return $this->order_service->createOrder(data: $data);
    }

    /**
     * @param UpdateSpecificationRequest $request
     * @return JsonResponse
     */
    public function updateOrder(UpdateSpecificationRequest $request)
    {
        $data = $request->getData();
        $order_id = $request->getOrderId();
        return $this->order_service->updateOrder(id: $order_id, data: $data);
    }

    public function addOrderItem(CreateOrderItemRequest $request)
    {
        $data=$request->getData();
        return$this->order_service->addOrderItem(data: $data);

    }

}
