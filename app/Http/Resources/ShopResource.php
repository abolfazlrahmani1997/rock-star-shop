<?php

namespace App\Http\Resources;

use App\Models\Shop;
use Illuminate\Http\Resources\Json\JsonResource;

class ShopResource extends JsonResource
{
    /**
     * transform resource into an array
     * @param $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable|void
     */
    public function toArray($request)
    {
        /**
         * @var Shop $this
         */
        return [
          'id' => $this->id,
          'user' => $this->whenLoaded('user', new UserResource($this->user)),
          'name' => $this->name,
          'description' => $this->description,
          'status' => $this->status,
        ];
    }
}
