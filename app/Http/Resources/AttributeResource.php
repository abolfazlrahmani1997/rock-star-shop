<?php

namespace App\Http\Resources;

use App\Models\Attribute;
use App\Models\AttributeProduct;
use Illuminate\Http\Resources\Json\JsonResource;

class AttributeResource extends JsonResource
{
    /**
     * transform resource into an array
     * @param $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable|void
     */
    public function toArray($request)
    {
        /**
         * @var AttributeProduct $this
         */

        return [
            'id' => $this->id,
            'title' => $this->title,
        ];
    }
}
