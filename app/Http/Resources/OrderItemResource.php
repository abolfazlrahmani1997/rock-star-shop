<?php

namespace App\Http\Resources;

use App\Models\Specification;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    /**
     * transform resource into an array
     * @param $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable|void
     */
    public function toArray($request)
    {
        /**
         * @var Specification $this
         */

        return [
          'id' => $this->id,
            'name'=>$this->whenLoaded('items', $this->items->product->title),
          'attribute_type' =>$this->items->attribute->title,
          'attribute_value' =>$this->items->attribute_value,
          'num' =>$this->number,
        ];
    }
}
