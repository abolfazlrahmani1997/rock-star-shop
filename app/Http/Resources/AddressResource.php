<?php

namespace App\Http\Resources;

use App\Models\Address;
use App\Models\Category;
use App\Models\Product;
use App\Models\Shop;
use App\Models\SubCategory;
use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * transform resource into an array
     * @param $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable|void
     */
    public function toArray($request)
    {
        /**
         * @var Address $this
         */
        return [
          'id' => $this->id,
          'city' => $this->whenLoaded('city', new CityResource($this->city)),
          'street' => $this->street,
          'address' => $this->address,
          'post_code' => $this->post_code,
          'users' => $this->whenLoaded('users', new UserResource($this->users)),
          'shops' => $this->whenLoaded('shops', new ShopResource($this->shops)),
        ];
    }
}
