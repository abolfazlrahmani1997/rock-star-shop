<?php

namespace App\Http\Resources;
use App\Models\Media;
use Illuminate\Http\Resources\Json\JsonResource;

class MediaResource extends JsonResource
{
    /**
     * transform resource into an array
     * @param $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable|void
     */
    public function toArray($request)
    {
        /**
         * @var Media $this
         */
        return [
          'id' => $this->id,
          'path' => $this->path,
          'status' => $this->status,
          'users' => $this->whenLoaded('users', new UserResource($this->users)),
          'shops' => $this->whenLoaded('shops', new ShopResource($this->shops)),
          'products' => $this->whenLoaded('products', new ProductResource($this->products)),
        ];
    }
}
