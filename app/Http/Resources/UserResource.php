<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * transform resource into an array
     * @param $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable|void
     */
    public function toArray($request)
    {
        /**
         * @var User $this
         */
        return [
          'id' => $this->id,
          'name' => $this->name,
          'lastname' => $this->lastname,
          'mobile' => $this->mobile,
        ];
    }
}
