<?php

namespace App\Http\Resources;

use App\Models\Brand;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * transform resource into an array
     * @param $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable|void
     */
    public function toArray($request)
    {
        /**
         * @var Product $this
         */

        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
           'attributeProducts' => $this->whenLoaded('attributeProducts',AttributeProductResource::collection($this->attributeProducts)),
        ];
    }
}
