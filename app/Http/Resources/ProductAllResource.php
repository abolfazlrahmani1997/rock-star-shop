<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductAllResource extends JsonResource
{
    /**
     * transform resource into an array
     * @param $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable|void
     */
    public function toArray($request)
    {
        /**
         * @var Product $this
         */

        return [
            'id' => $this->id,
            'sub_category' => $this->whenLoaded('subCategory', new SubCategoryResource($this->subCategory)),
            'brand' => $this->whenLoaded('brand', new BrandResource($this->brand)),
            'title' => $this->title,
            'description' => $this->description,
            'price' => $this->price,
            'images' => $this->whenLoaded('attributeProducts',AttributeProductResource::collection($this->attributeProducts)),
        ];
    }
}
