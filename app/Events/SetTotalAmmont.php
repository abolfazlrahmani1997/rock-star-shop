<?php

namespace App\Events;

use App\Models\AttributeProduct;
use App\Models\OrderProduct;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SetTotalAmmont
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public int $order_id;
    public OrderProduct $order_item_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(int $order_id,OrderProduct $order_item_id)
    {
        $this->order_id=$order_id;
        $this->order_item_id=$order_item_id;
    }


}
