<?php

namespace App\Events;

use App\Models\Product;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SpecificationEvent
{
    /**
     * Dispatch able
     */
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     *
     */
    public Product $product;

    /**
     * Array File
     * @var array
     */
    public array $data;

    public function __construct(Product $product, array $data = null)
    {
        $this->product = $product;
        $this->data = $data;
    }
}
