<?php

namespace App\Events;

use App\Models\Interfaces\HasMediaInterface;
use App\Models\Product;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AttributeEvent
{
    /**
     * Dispatch able
     */
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Upload file
     * @var HasMediaInterface
     */
    public HasMediaInterface $product;

    /**
     * Array File
     * @var array
     */
    public array $data;

    public function __construct(Product $product, array $data = null)
    {
        $this->product = $product;
        $this->data = $data;
    }
}
