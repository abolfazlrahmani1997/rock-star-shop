<?php

namespace App\Events;

use App\Models\Interfaces\HasMediaInterface;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UploadFileEvent
{
    /**
     * Dispatch able
     */
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Upload file
     * @var HasMediaInterface
     */
    public HasMediaInterface $has_media;

    /**
     * Array File
     * @var array
     */
    public array $data;

    public function __construct(HasMediaInterface $has_media, array $data = null)
    {

        $this->has_media = $has_media;
        $this->data = $data;
    }
}
