<?php

namespace App\Discount\Strategies;


use App\Discount\Strategies\Interfaces\StrategyInterface;
use App\Models\Discount;
use App\Models\Order;


class DiscountPattern implements StrategyInterface
{

    /**
     * Set DiscountAble
     * @param Discount $discount
     * @param Order $order
     * @return Order
     */
    public function OrderDiscount(Discount $discount, Order $order): Order
    {
        $users_id = $discount->usersId();
        $products_id = $discount->productsId();

        if (in_array($order->user_id, $users_id)) {

            foreach ($order->products as $product) {
                if (in_array($product->pivot->product_id, $products_id)) {
                    $product->pivot->price = $product->pivot->price - (($product->pivot->price * $discount->percent)/100);
                    $product->pivot->save();
                }
            }
        }

    }
}
