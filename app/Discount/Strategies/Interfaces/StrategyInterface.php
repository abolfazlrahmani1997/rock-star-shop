<?php

namespace App\Discount\Strategies\Interfaces;

use App\Models\Discount;
use App\Models\Order;

interface StrategyInterface
{
    /**
     * Set DiscountAble
     * @param Discount $discount_able
     * @param Order $order
     * @return Order
     */
    public function OrderDiscount(Discount $discount_able, Order $order): Order;
}
