<?php

namespace App\Discount;

use App\Discount\Strategies\Interfaces\StrategyInterface;

class DiscountService
{
    private StrategyInterface $strategy;

    public function __construct(StrategyInterface $strategy)
    {
        $this->strategy = $strategy;
    }

    public function discount()
    {
//        $this->strategy->OrderDiscount();

    }
}
