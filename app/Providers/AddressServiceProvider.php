<?php

namespace App\Providers;

use App\Repositories\AddressRepository;
use App\Repositories\Interfaces\AddressRepositoryInterface;
use App\Services\AddressService;
use App\Services\Interfaces\AddressServiceInterface;
use Illuminate\Support\ServiceProvider;

class AddressServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->bind(AddressRepositoryInterface::class, AddressRepository::class);
        $this->app->bind(AddressServiceInterface::class, AddressService::class);
    }
}
