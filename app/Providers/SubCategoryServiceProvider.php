<?php

namespace App\Providers;

use App\Repositories\Interfaces\SubCategoryRepositoryInterface;
use App\Repositories\SubCategoryRepository;
use App\Services\Interfaces\SubCategoryServiceInterface;
use App\Services\SubCategoryService;
use Illuminate\Support\ServiceProvider;

class SubCategoryServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->bind(SubCategoryRepositoryInterface::class, SubCategoryRepository::class);
        $this->app->bind(SubCategoryServiceInterface::class, SubCategoryService::class);
    }
}
