<?php

namespace App\Providers;

use App\Events\AttributeEvent;
use App\Events\SetTotalAmmont;
use App\Events\SpecificationEvent;
use App\Events\UploadFileEvent;
use App\Listeners\AttributeListener;
use App\Listeners\SetTotalAmountListener;
use App\Listeners\SpecificationListener;
use App\Listeners\UploadFileListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        UploadFileEvent::class => [
            UploadFileListener::class
        ],
        AttributeEvent::class => [
            AttributeListener::class
        ],
        SpecificationEvent::class => [
            SpecificationListener::class
        ],
        SetTotalAmmont::class=>[
            SetTotalAmountListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
