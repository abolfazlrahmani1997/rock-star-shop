<?php

namespace App\Providers;

use App\Repositories\Interfaces\MediaRepositoryInterface;
use App\Repositories\MediaRepository;
use App\Services\Interfaces\MediaServiceInterface;
use App\Services\Interfaces\StorageServiceInterface;
use App\Services\MediaService;
use App\Services\StorageService;
use Illuminate\Support\ServiceProvider;

class MediaServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->bind(MediaRepositoryInterface::class, MediaRepository::class );
        $this->app->bind(MediaServiceInterface::class, MediaService::class );

        $this->app->bind(StorageServiceInterface::class, StorageService::class);
    }
}
