<?php

namespace App\Providers;

use App\Repositories\AttributeProductRepository;
use App\Repositories\AttributeRepository;
use App\Repositories\Interfaces\AttributeProductRepositoryInterface;
use App\Repositories\Interfaces\AttributeRepositoryInterface;
use App\Repositories\Interfaces\OrderProductRepositoryInterface;
use App\Repositories\Interfaces\SpecificationRepositoryInterface;
use App\Repositories\OrderProductRepository;
use App\Repositories\SpecificationRepository;
use App\Services\AttributeProductService;
use App\Services\AttributeService;
use App\Services\AuthService;
use App\Services\Interfaces\AttributeProductServiceInterface;
use App\Services\Interfaces\AttributeServiceInterface;
use App\Services\Interfaces\AuthServiceInterface;
use App\Services\Interfaces\SpecificationServiceInterface;
use App\Services\SpecificationService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
     $this->app->bind(AttributeServiceInterface::class, AttributeService::class);
     $this->app->bind(AttributeRepositoryInterface::class, AttributeRepository::class);
     $this->app->bind(OrderProductRepositoryInterface::class, OrderProductRepository::class);
     $this->app->bind(AttributeProductRepositoryInterface::class, AttributeProductRepository::class);
     $this->app->bind(AttributeProductServiceInterface::class, AttributeProductService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
