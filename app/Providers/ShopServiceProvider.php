<?php

namespace App\Providers;

use App\Repositories\Interfaces\ShopRepositoryInterface;
use App\Repositories\ShopRepository;
use App\Services\Interfaces\ShopServiceInterface;
use App\Services\ShopService;
use Illuminate\Support\ServiceProvider;

class ShopServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->bind(ShopRepositoryInterface::class, ShopRepository::class);
        $this->app->bind(ShopServiceInterface::class, ShopService::class);
    }
}
