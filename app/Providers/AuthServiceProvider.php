<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use App\Services\AuthService;
use App\Services\Interfaces\AuthServiceInterface;
use Carbon\Carbon;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
//        Passport::loadKeysFrom(__DIR__.'/../secrets/oauth');
        $this->app->bind(AuthServiceInterface::class, AuthService::class);
//        Passport::$registersRoutes;
        Passport::personalAccessTokensExpireIn(Carbon::now()->addHours(24));
    }
}
