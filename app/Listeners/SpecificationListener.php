<?php

namespace App\Listeners;

use App\Events\AttributeEvent;
use App\Events\SpecificationEvent;
use App\Events\UploadFileEvent;
use App\Repositories\Interfaces\AttributeRepositoryInterface;
use App\Repositories\Interfaces\MediaRepositoryInterface;
use App\Repositories\Interfaces\SpecificationRepositoryInterface;
use Illuminate\Contracts\Queue\ShouldQueue;

class SpecificationListener implements ShouldQueue
{
    /**
     * @var SpecificationRepositoryInterface
     */
    private SpecificationRepositoryInterface $specification_repository;

    /**
     * Create the event listener
     */
    public function __construct(SpecificationRepositoryInterface $specification_repository)
    {
        $this->specification_repository = $specification_repository;
    }

    /**
     * Handle the event
     * @param SpecificationEvent $event
     * @return void
     */
    public function handle(SpecificationEvent $event)
    {
      foreach ($event->data as $specification)
      {
          $specification['product_id'] = $event->product->id;
          $this->insertToSpecification(data: $specification);
      }
    }

    /**
     * Insert To Product
     */
    public function insertToSpecification(array $data)
    {
        $specification = $this->specification_repository->createSpecification(data: $data);
    }

}
