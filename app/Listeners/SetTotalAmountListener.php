<?php

namespace App\Listeners;

use App\Events\SetTotalAmmont;
use App\Models\AttributeProduct;
use App\Models\Order;
use App\Repositories\Interfaces\AttributeProductRepositoryInterface;
use App\Repositories\Interfaces\AttributeRepositoryInterface;
use App\Repositories\Interfaces\OrderRepositoryInterface;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SetTotalAmountListener
{
    private AttributeProductRepositoryInterface $attribute_repository;
    private OrderRepositoryInterface $order_repository;
    private ProductRepositoryInterface $product_reposiotry;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(AttributeProductRepositoryInterface $attributeRepository,ProductRepositoryInterface $productRepository,OrderRepositoryInterface $orderRepository)
    {

        $this->attribute_repository=$attributeRepository;
        $this->product_reposiotry=$productRepository;
        $this->order_repository=$orderRepository;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(SetTotalAmmont $event)
    {   $order=$this->order_repository->getOrder(id:$event->order_id,with_relation: true);
        $product=$order->products;
        $total=0;
        foreach ($product as $pr)
        {
          $pr=$pr->product->product;
          $total+=$pr->price;
        }
        $this->order_repository->updateOrder($order,data: ['total_amount'=>$total,'status'=>'pending']);
    }

    /**
     * Sum For Amount
     */
    private function sumTotal(Order $order)
    {

    }






}
