<?php

namespace App\Listeners;

use App\Events\AttributeEvent;
use App\Events\UploadFileEvent;
use App\Repositories\Interfaces\AttributeRepositoryInterface;
use App\Repositories\Interfaces\MediaRepositoryInterface;
use Illuminate\Contracts\Queue\ShouldQueue;

class AttributeListener implements ShouldQueue
{
    /**
     * @var AttributeRepositoryInterface
     */
    private AttributeRepositoryInterface $attribute_repository;

    /**
     * Create the event listener
     */
    public function __construct(AttributeRepositoryInterface $attribute_repository)
    {
        $this->attribute_repository = $attribute_repository;
    }

    /**
     * Handle the event
     * @param AttributeEvent $event
     * @return void
     */
    public function handle(AttributeEvent $event)
    {
      foreach ($event->data as $attribute)
      {

          $attribute['product_id'] = $event->product->id;
          $this->insertToDb(data: $attribute);
      }
    }

    /**
     * Insert To Product
     */
    public function insertToDb(array $data)
    {
        $attribute = $this->attribute_repository->createAttributeProduct(data: $data);
    }

}
