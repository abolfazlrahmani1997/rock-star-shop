<?php

namespace App\Listeners;

use App\Events\UploadFileEvent;
use App\Repositories\Interfaces\MediaRepositoryInterface;
use Illuminate\Contracts\Queue\ShouldQueue;

class UploadFileListener implements ShouldQueue
{
    /**
     * @var MediaRepositoryInterface
     */
    private MediaRepositoryInterface $media_repository;

    /**
     * Create the event listener
     * @param MediaRepositoryInterface $media_repository
     */
    public function __construct(MediaRepositoryInterface $media_repository)
    {
        $this->media_repository = $media_repository;
    }

    /**
     * Handle the event
     * @param UploadFileEvent $event
     * @return void
     */
    public function handle(UploadFileEvent $event)
    {
        $media = $this->media_repository->createMedia(mediaable: $event->has_media,
            data: ['path' => $event->data['path']]);
    }

}
