<?php

namespace App\Models\Traits;

use App\Models\DiscountAttributes;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait HasDiscountAble
{
    /**
     * Return All Discounts
     * @return MorphMany
     */
    public function HasDiscounts(): MorphMany
    {
        return $this->morphMany(DiscountAttributes::class, 'discountable', 'discountable_type', 'discountable_id', 'id');
    }
}
