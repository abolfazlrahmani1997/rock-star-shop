<?php

namespace App\Models\Traits;

use App\Models\Media;
use App\Models\Mediaable;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait HasMedia
{
    /**
     * Return All Media
     * @return MorphMany
     */
    public function HasMedias():MorphMany
    {
        return $this->morphMany(Media::class, 'mediaable', 'mediaable_type', 'mediaable_id', 'id');
    }
}
