<?php

namespace App\Models\Traits;

use App\Models\Address;
use App\Models\Media;
use App\Models\Mediaable;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait HasAddress
{
    /**
     * Return All Media
     * @return MorphMany
     */
    public function HasAddresses():MorphMany
    {
        return $this->morphMany(Address::class, 'addressable', 'addressable_type', 'addressable_id', 'id');
    }
}
