<?php

namespace App\Models\Traits;

use App\Models\Payment;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait HasPayment
{
    /**
     * Return All Media
     * @return MorphMany
     */
    public function HasPayments():MorphMany
    {
        return $this->morphMany(Payment::class, 'paymentable', 'paymentable_type', 'paymentable_id', 'id');
    }
}
