<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * define AttributeProduct Class
 * @property integer $id
 * @property integer $attribute_id
 * @property integer $product_id
 * @property string $attribute_value
 * @property double $price
 */
class AttributeProduct extends Model
{
    /**
     * set Factory
     */
    use HasFactory;

    /**
     * set table name
     * @var string
     */
    protected $table = 'attribute_product';

    /**
     * set off timestamp
     * @var bool
     */
    public $timestamps = false;

    /**
     * set fillable
     * @var string[]
     */
    protected $fillable = [
        'id',
        'attribute_id',
        'product_id',
        'attribute_value',
    ];

    /**
     * get attribute
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function attribute()
    {
        return $this->belongsTo(Attribute::class, 'attribute_id', 'id');
    }

    /**
     * get product
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
