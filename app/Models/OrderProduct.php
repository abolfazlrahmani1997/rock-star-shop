<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * define OrderProduct Class
 * @property integer $id
 * @property integer $product_id
 * @property integer $order_id
 * @property integer $shop_id
 * @property integer $number
 * @property integer $price
 */
class OrderProduct extends Model
{
    /**
     * set Factory
     */
    use HasFactory;

    /**
     * set table name
     * @var string
     */
    protected $table = 'order_product';

    /**
     * set off timestamp
     * @var bool
     */
    public $timestamps = false;

    /**
     * set fillable
     * @var string[]
     */
    protected $fillable = [
        'id',
        'product_id',
        'order_id',
        'number',
    ];

    /**
     * get products
     * pivotTable
     */
    public function items(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(AttributeProduct::class, 'attribute_id', 'id');
    }

    public function order(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }


}
