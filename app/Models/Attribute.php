<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * define Attribute Class
 * @property integer $id
 * @property string $title
 * @property double $price
 */
class Attribute extends Model
{
    /**
     * set Factory
     */
    use HasFactory;

    /**
     * set table name
     * @var string
     */
    protected $table = 'attributes';

    /**
     * set off timestamp
     * @var bool
     */
    public $timestamps = false;

    /**
     * set fillable
     * @var string[]
     */
    protected $fillable = [
        'id',
        'title',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributeProducts()
    {
        return $this->hasMany(AttributeProduct::class, 'attribute_id', 'id');
    }
}
