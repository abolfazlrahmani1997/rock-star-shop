<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\Traits\HasAddress;
use App\Models\Traits\HasMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * define User Class
 * @property integer $id
 * @property string $name
 * @property string $lastname
 * @property string $mobile
 * @property string $email
 * @property string $password
 */

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasMedia, HasAddress;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'lastname',
        'mobile',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
//        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * get orders
     */
    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id', 'id');
    }
    /**
     * get comments
     */
    public function comments()
    {
        return $this->hasMany(Comment::class, 'user_id', 'id');
    }
    /**
     * get shops
     */
    public function shops()
    {
        return $this->hasMany(Shop::class, 'user_id', 'id');
    }
}
