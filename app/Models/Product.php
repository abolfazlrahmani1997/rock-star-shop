<?php

namespace App\Models;

use App\Models\Interfaces\HasDiscountInterface;
use App\Models\Interfaces\HasMediaInterface;
use App\Models\Traits\HasDiscountAble;
use App\Models\Traits\HasMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * define Product Class
 * @property integer $id
 * @property integer $sub_category_id
 * @property integer $brand_id
 * @property string $title
 * @property string $description
 * @property integer $price
 */
class Product extends Model implements HasMediaInterface, HasDiscountInterface
{
    /**
     * set Factory & media
     */
    use HasFactory, HasMedia, HasDiscountAble;

    /**
     * set table name
     * @var string
     */
    protected $table = 'products';

    /**
     * set off timestamp
     * @var bool
     */
    public $timestamps = false;

    /**
     * set fillable
     * @var string[]
     */
    protected $fillable = [
        'id',
        'sub_category_id',
        'brand_id',
        'title',
        'description',
        'price',
    ];


    /**
     * get subCategory
     */
    public function subCategory()
    {
        return $this->belongsTo(SubCategory::class, 'sub_category_id', 'id');
    }

    /**
     * get specification
     */
    public function specifications()
    {
        return $this->hasMany(Specification::class, 'product_id', 'id');
    }

    /**
     * get shop product
     */
    public function shopProduct()
    {
        return $this->belongsToMany(Shop::class, 'shop_product', 'product_id','shop_id','id')->withPivot(['price']);
    }

    /**
     * get events
     * pivotTable
     */
    public function events()
    {
        return $this->belongsToMany(Event::class, 'event_product', 'event_id', 'id');
    }

    /**
     * get orders
     * pivotTable
     */
    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_product', 'order_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributeProducts()
    {
        return $this->hasMany(AttributeProduct::class, 'product_id', 'id');
    }

    /**
     * get brand
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id', 'id');
    }
}
