<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * define Order Class
 * @property integer $id
 * @property integer $user_id
 * @property integer $total_amount
 * @property string $status
 */

class Order extends Model
{
    /**
     * set Factory
     */
    use HasFactory;

    /**
     * set table name
     * @var string
     */
    protected $table = 'orders';

    /**
     * set off timestamp
     * @var bool
     */
    public $timestamps = true;

    /**
     * set fillable
     * @var string[]
     */
    protected $fillable = [
        'id',
        'user_id',
        'total_amount',
        'status',
    ];


    /**
     * get user
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * get products
     * pivotTable
     */
    public function products()
    {
        return $this->hasMany(OrderProduct::class, 'order_id', 'id');
    }

    /**
     * get peyments
     */
    public function payments()
    {
        return $this->hasMany(Payment::class, 'order_id', 'id');
    }
}
