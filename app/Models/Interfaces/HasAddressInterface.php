<?php

namespace App\Models\Interfaces;

use Illuminate\Database\Eloquent\Relations\MorphMany;

interface HasAddressInterface
{

    /**
     * Return All Image
     * @return MorphMany
     */
    public function HasAddresses():MorphMany;
}
