<?php

namespace App\Models\Interfaces;

use Illuminate\Database\Eloquent\Relations\MorphMany;

interface HasPaymentInterface
{

    /**
     * Return All Image
     * @return MorphMany
     */
    public function HasPayment():MorphMany;
}
