<?php

namespace App\Models\Interfaces;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;

interface HasDiscountInterface
{
    /**
     * Return All Image
     * @return MorphMany
     */
    public function HasDiscounts():MorphMany;
}
