<?php

namespace App\Models\Interfaces;

use Illuminate\Database\Eloquent\Relations\MorphMany;

interface HasMediaInterface
{

    /**
     * Return All Image
     * @return MorphMany
     */
    public function HasMedias():MorphMany;
}
