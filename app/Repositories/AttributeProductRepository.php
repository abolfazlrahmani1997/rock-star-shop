<?php

namespace App\Repositories;

use App\Models\AttributeProduct;
use App\Repositories\Interfaces\AttributeProductRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class AttributeProductRepository implements AttributeProductRepositoryInterface
{

    /**
     * get and find one attribute_product
     * @param int $od
     * @param bool $with_relation
     * @return AttributeProduct
     */
    public function getAttributeProduct(int $id, bool $with_relation): AttributeProduct
    {
        return $this->fetchRelation(with_relation: $with_relation)
            ->findOrFail(id: $id);
    }

    /**
     * get all attribute_product
     * @param bool $with_relation
     * @return Collection
     */
    public function getAllAttributeProduct(bool $with_relation): Collection
    {
        return $this->fetchRelation(with_relation: $with_relation)
            ->get();
    }

    /**
     * create attribute_product
     * @param array $data
     * @return AttributeProduct|false
     */
    public function createAttributeProduct(array $data): AttributeProduct|false
    {
        $attribute_product = new AttributeProduct();
        $attribute_product->product_id = $data['product_id'];
        $attribute_product->attribute_id = $data['attribute_id'];
        $attribute_product->attribute_value = $data['attribute_value'];
        $attribute_product->price=$data['price'];
        try {
            $attribute_product->save();
        } catch (\Exception $q) {
            Log::alert('cant create attribute_product' . $q->getMessage());
            return false;
        }
        return $attribute_product;
    }

    /**
     * update attribute_product
     * @param AttributeProduct $attribute_product
     * @param array $data
     * @return AttributeProduct|false
     */
    public function updateAttributeProduct(AttributeProduct $attribute_product, array $data): AttributeProduct|false
    {
        if (isset($data['product_id'])) {
            $attribute_product->product_id = $data['product_id'];
        }
        if (isset($data['attribute_id'])) {
            $attribute_product->attribute_id = $data['attribute_id'];
        }
        if (isset($data['attribute_value'])) {
            $attribute_product->attribute_value = $data['attribute_value'];
        }
        try {
            $attribute_product->save();
        } catch (\Exception $q) {
            Log::alert('cant update attribute_product' . $q->getMessage());
            return false;
        }
        return $attribute_product;
    }

    /**
     * delete attribute_product
     * @param AttributeProduct $attribute_product
     * @return bool
     */
    public function deleteAttributeProduct(AttributeProduct $attribute_product): bool
    {
        try {
            $attribute_product->deleteOrFail();
        } catch (\Exception $q) {
            Log::alert('cant update attribute_product' . $q->getMessage());
            return false;
        }
        return true;
    }

    /**
     * @param bool $with_relation
     * @return Builder|\Illuminate\Support\HigherOrderWhenProxy|mixed
     */
    public function fetchRelation(bool $with_relation)
    {
        return AttributeProduct::query()
            ->when($with_relation, function (Builder $query) {
                return $query->with(['product']);
            });
    }
}
