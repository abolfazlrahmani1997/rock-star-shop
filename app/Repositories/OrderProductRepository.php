<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\User;
use App\Repositories\Interfaces\OrderProductRepositoryInterface;
use App\Repositories\Interfaces\OrderRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class OrderProductRepository implements OrderProductRepositoryInterface
{
    /**
     * get one user
     * @param int|null $id
     * @param bool $with_relation
     * @return Order
     */
    public function getOrderProduct(int $id = null, bool $with_relation = false): Order
    {
        return $this->withRelation(with_relation: $with_relation)
            ->findOrFail(id: $id);
    }

    /**
     * get all user
     * @param bool $with_relation
     * @return Collection
     */
    public function getAllOrderProducts(bool $with_relation = false): Collection
    {
        return $this->withRelation(with_relation: $with_relation)
            ->get();
    }

    /**
     * create DiscountAttributes
     * @param array $data
     * @return User|false
     */
    public function createOrderProduct(array $data): OrderProduct|false
    {
       $order = new OrderProduct();
       $order->order_id = $data['order_id'];
       $order->attribute_id = $data['attribute_id'];
        try {
            $order->save();
        } catch (\Exception $q) {
            Log::alert($q->getMessage().'cant create order Product');
            return false;
        }
        return $order;
    }

    /**
     * update DiscountAttributes
     * @param OrderProduct $user
     * @param array $data
     * @return User|false
     */
    public function updateOrderProduct(OrderProduct $order, array $data): OrderProduct|false
    {
        if (isset($data['order_id'])) {
            $order->order_id = $data['order_id'];
        }
        if (isset($data['product_id'])) {
            $order->order_id = $data['order_id'];
        }

        if (isset($data['attribute_id'])) {
            $order->attribute_id = $data['attribute_id'];
        }
        try {
            $order->save();
        } catch (\Exception $q) {
            Log::alert('cant update order');
            return false;
        }
        return $order;
    }

    /**
     * fetch relation
     */
    private function withRelation(bool $with_relation = false)
    {
        return OrderProduct::query()
            ->when($with_relation, function (Builder $query) {
                $query->with(['user', 'products', 'payments']);
            });
    }
}
