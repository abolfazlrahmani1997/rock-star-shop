<?php

namespace App\Repositories;

use App\Models\Order;
use App\Models\User;
use App\Repositories\Interfaces\OrderRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class OrderRepository implements OrderRepositoryInterface
{
    /**
     * get one user
     * @param int|null $id
     * @param bool $with_relation
     * @return Order
     */
    public function getOrder(int $id = null, bool $with_relation = false): Order
    {
        return $this->withRelation(with_relation: $with_relation)
            ->findOrFail(id: $id);
    }

    /**
     * get all user
     * @param bool $with_relation
     * @return Collection
     */
    public function getAllOrders(bool $with_relation = false): Collection
    {
        return $this->withRelation(with_relation: $with_relation)
            ->get();
    }

    /**
     * create DiscountAttributes
     * @param array $data
     * @return User|false
     */
    public function createOrder(array $data): Order|false
    {
       $order = new Order();
       $order->user_id = $data['user_id'];
       $order->total_amount = $data['total_amount'];

        try {
            $order->save();
        } catch (\Exception $q) {
            Log::alert($q->getMessage().'cant create order');
            return false;
        }
        return $order;
    }

    /**
     * update DiscountAttributes
     * @param Order $user
     * @param array $data
     * @return User|false
     */
    public function updateOrder(Order $order, array $data): Order|false
    {
        if (isset($data['user_id'])) {
            $order->user_id = $data['user_id'];
        }
        if (isset($data['total_amount'])) {
            $order->total_amount = $data['total_amount'];
        }
        if (isset($data['status'])) {
            $order->status = $data['status'];
        }
        try {
            $order->save();
        } catch (\Exception $q) {
            Log::alert('cant update order');
            return false;
        }
        return $order;
    }

    /**
     * fetch relation
     */
    private function withRelation(bool $with_relation = false)
    {
        return Order::query()
            ->when($with_relation, function (Builder $query) {
                $query->with(['user', 'products','products.items','products.items.product']);
            });
    }
}
