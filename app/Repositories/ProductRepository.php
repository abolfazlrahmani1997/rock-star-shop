<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class ProductRepository implements ProductRepositoryInterface
{

    /**
     * Get Product
     * @param int|null $id
     * @param bool $with_relation
     * @return Product
     */
    public function getProduct(int $id = null, bool $with_relation = false): Product
    {
        return $this->fetchRelation(with_relation: $with_relation)
            ->findOrFail(id: $id);
    }

    /**
     * Get All Product
     * @param bool $with_relation
     * @return Collection
     */
    public function getAllProducts(array $data = null, bool $with_relation = false): Collection
    {
        return $this->fetchRelation(with_relation: true)
            ->when(isset($data['title']), fn(Builder $query) => $query->where('title', 'like', GeneralHelper::getRegular($data['title'], "%")))
            ->get();
    }

    /**
     * Create Product
     * @param array $data
     * @return Product|false
     */
    public function createProduct(array $data): Product|false
    {

        $product = new Product();
        $product->title = $data['title'];
        $product->description = $data['description'];
        $product->price = $data['price'];
        try {
            $product->save();
        } catch (\Exception $q) {
            Log::alert('cant create product' . $q->getMessage());
            return false;
        }
        return $product;
    }

    /**
     * Update Product
     * @param Product $product
     * @param array $data
     * @return Product|false
     */
    public function updateProduct(Product $product, array $data): Product|false
    {
        if (isset($data['sub_category_id'])) {
            $product->sub_category_id = $data['sub_category_id'];
        }
        if (isset($data['brand_id'])) {
            $product->brand_id = $data['brand_id'];
        }
        if (isset($data['title'])) {
            $product->title = $data['title'];
        }
        if (isset($data['description'])) {
            $product->description = $data['description'];
        }
        if (isset($data['price'])) {
            $product->price = $data['price'];
        }
        try {
            $product->save();
        } catch (\Exception $q) {
            Log::alert('cant create product' . $q->getMessage());
            return false;
        }
        return $product;
    }

    /**
     * Delete Product
     * @param Product $product
     * @return Product|false
     */
    public function deleteProduct(Product $product):bool
    {
        try {
            $product->deleteOrFail();
        } catch (\Exception $q) {
            Log::alert('cant delete product' . $q->getMessage());
            return false;
        }
        return true;
    }

    /**
     * fetch Relation
     * @param bool $with_relation
     * @return Builder|\Illuminate\Support\HigherOrderWhenProxy|mixed
     */
    private function fetchRelation(bool $with_relation)
    {
        return Product::query()
            ->when($with_relation, function (Builder $query) {
             return  $query->with([ 'attributeProducts','attributeProducts.attribute']);
            });
    }
}
