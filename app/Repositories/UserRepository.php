<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\DiscountAttributeRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use mysql_xdevapi\Exception;

class UserRepository implements UserRepositoryInterface
{

    /**
     * get one user
     * @param int|null $id
     * @param bool $with_relation
     * @return User
     */
    public function getUser(int $id = null, array $data = null, bool $with_relation = false): User
    {
        $email = $data['email'];
        return $this->fetchRelation(with_relation: $with_relation)
            ->when(isset($email), function (Builder $query) use ($email) {
          return $query->where('email', '=', $email);
             })
            ->when(isset($id), function (Builder $query) use ($id) {
            return $query->where('id', '=', $id);
            })->firstOrFail();
    }

    /**
     * get all user
     * @param bool $with_relation
     * @return Collection
     */
    public function getAllUser(bool $with_relation = false): Collection
    {
        return $this->fetchRelation(with_relation: $with_relation)
            ->get();
    }

    /**
     * create user
     * @param array $data
     * @return User|false
     */
    public function createUser(array $data): User|false
    {
        $user = new User();
        $user->name = $data['name'];
        $user->lastname = $data['lastname'];
        $user->mobile = $data['mobile'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        try {
            $user->save();
        } catch (QueryException $q) {
            Log::alert('cant create user' . $q->getMessage());
            return false;
        }
        return $user;
    }

    /**
     * update user
     * @param User $user
     * @param array $data
     * @return User|false
     */
    public function updateUser(User $user, array $data): User|false
    {
        if(isset($data['name'])) {
            $user->name = $data['name'];
        }
        if(isset($data['lastname'])) {
            $user->lastname = $data['lastname'];
        }
        if(isset($data['mobile'])) {
            $user->mobile = $data['mobile'];
        }
        if(isset($data['email'])) {
            $user->email = $data['email'];
        }
        if(isset($data['password'])) {
            $user->password = Hash::make($data['password']);
        }
        try {
            $user->save();
        } catch (Exception $q) {
            Log::alert('cant user update' . $q->getMessage());
            return false;
        }

        return $user;
    }

    private function fetchRelation(bool $with_relation)
    {
        return User::query()
            ->when($with_relation, function (Builder $query) {
                $query->with(['orders', 'comments', 'shops', 'HasMedia', 'HasAddress']);
            });
    }
}
