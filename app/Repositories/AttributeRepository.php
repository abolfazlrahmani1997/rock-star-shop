<?php

namespace App\Repositories;

use App\Models\Attribute;
use App\Models\AttributeProduct;
use App\Repositories\Interfaces\AttributeRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class AttributeRepository implements AttributeRepositoryInterface
{

    /**
     * get and find one attribute
     * @param int $id
     * @param bool $with_relation
     * @return Attribute
     */
    public function getAttribute(int $id, bool $with_relation): Attribute
    {
        return $this->fetchRelation(with_relation: $with_relation)
            ->findOrFail(id: $id);
    }

    /**
     * get all attribute
     * @param bool $with_relation
     * @return Collection
     */
    public function getAllAttribute(bool $with_relation): Collection
    {
        return $this->fetchRelation(with_relation: $with_relation)
            ->get();
    }

    /**
     * create attribute
     * @param array $data
     * @return Attribute|false
     */
    public function createAttribute(array $data): Attribute|false
    {
        $attribute = new Attribute();
        $attribute->title = $data['title'];
        $attribute->price=$data['price'];
        try {
            $attribute->save();
        } catch (\Exception $q) {
            Log::alert('cant create category' . $q->getMessage());
            return false;
        }
        return $attribute;
    }

    /**
     * create attribute
     * @param array $data
     * @return Attribute|false
     */
    public function createAttributeProduct(array $data): AttributeProduct|false
    {
        $attribute = new AttributeProduct();
        $attribute->product_id = $data['product_id'];
        $attribute->attribute_id = $data['attribute_id'];
        $attribute->attribute_value = $data['attribute_value'];
        $attribute->price=$data['price'];
        try {
            $attribute->save();
        } catch (\Exception $q) {
            Log::alert('cant create category' . $q->getMessage());
            return false;
        }
        return $attribute;
    }

    /**
     * update attribute
     * @param Attribute $attribute
     * @param array $data
     * @return Attribute|false
     */
    public function updateAttribute(Attribute $attribute, array $data): Attribute|false
    {
        if (isset($data['title'])) {
            $attribute->title = $data['title'];
        }
        try {
            $attribute->save();
        } catch (\Exception $q) {
            Log::alert('cant update category' . $q->getMessage());
            return false;
        }
        return $attribute;
    }

    /**
     * delete attribute
     * @param Attribute $attribute
     * @return bool
     */
    public function deleteAttribute(Attribute $attribute): bool
    {
        try {
            $attribute->deleteOrFail();
        } catch (\Exception $q) {
            Log::alert('cant update category' . $q->getMessage());
            return false;
        }
        return true;
    }

    /**
     * fetch relation
     * @param $with_relation
     * @return Builder|\Illuminate\Support\HigherOrderWhenProxy|mixed
     */
    public function fetchRelation($with_relation)
    {
        return Attribute::query()
            ->when($with_relation, function (Builder $query) {
                $query->with(['attributeProducts']);
            });
    }
}
