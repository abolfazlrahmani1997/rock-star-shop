<?php

namespace App\Repositories\Interfaces;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface UserRepositoryInterface
{
    /**
     * get one user
     * @param int|null $id
     * @param bool $with_relation
     * @return User
     */
    public function getUser(int $id = null, array $data = null, bool $with_relation = false): User;

    /**
     * get all user
     * @param bool $with_relation
     * @return Collection
     */
    public function getAllUser(bool $with_relation = false): Collection;

    /**
     * create user
     * @param array $data
     * @return User|false
     */
    public function createUser(array $data): User|false;

    /**
     * update user
     * @param User $user
     * @param array $data
     * @return User|false
     */
    public function updateUser(User $user, array $data): User|false;

}
