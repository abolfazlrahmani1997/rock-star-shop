<?php

namespace App\Repositories\Interfaces;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface ProductRepositoryInterface
{
    /**
     * get one user
     * @param int|null $id
     * @param bool $with_relation
     * @return Product
     */
    public function getProduct(int $id = null, bool $with_relation = false): Product;

    /**
     * get all user
     * @param bool $with_relation
     * @return Collection
     */
    public function getAllProducts(array $data = null, bool $with_relation = false): Collection;

    /**
     * create user
     * @param array $data
     * @return User|false
     */
    public function createProduct(array $data): Product|false;

    /**
     * update user
     * @param User $user
     * @param array $data
     * @return User|false
     */
    public function updateProduct(Product $product, array $data): Product|false;


    /**
     * delete Product
     * @param User $user
     * @param array $data
     * @return User|false
     */
    public function deleteProduct(Product $product): bool;

}
