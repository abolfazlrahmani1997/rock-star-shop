<?php

namespace App\Repositories\Interfaces;

use App\Models\Discount;
use App\Models\DiscountAttributes;
use App\Models\Interfaces\HasDiscountInterface;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface OrderProductRepositoryInterface
{
    /**
     * get one user
     * @param int|null $id
     * @param bool $with_relation
     * @return Order
     */
    public function getOrderProduct(int $id = null, bool $with_relation = false): Order;

    /**
     * get all user
     * @param bool $with_relation
     * @return Collection
     */
    public function getAllOrderProducts( bool $with_relation = false): Collection;

    /**
     * create DiscountAttributes
     * @param array $data
     * @return User|false
     */
    public function createOrderProduct(array $data): OrderProduct|false;

    /**
     * update DiscountAttributes
     * @param OrderProduct $user
     * @param array $data
     * @return User|false
     */
    public function updateOrderProduct(OrderProduct $order, array $data): OrderProduct|false;


}
