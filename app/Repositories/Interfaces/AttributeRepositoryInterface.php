<?php

namespace App\Repositories\Interfaces;

use App\Models\Attribute;
use App\Models\AttributeProduct;
use Illuminate\Database\Eloquent\Collection;

interface AttributeRepositoryInterface
{
    /**
     * get and find one attribute
     * @param int $od
     * @param bool $with_relation
     * @return Attribute
     */
    public function getAttribute(int $id, bool $with_relation): Attribute;

    /**
     * get all attribute
     * @param bool $with_relation
     * @return Collection
     */
    public function getAllAttribute(bool $with_relation): Collection;

    /**
     * create attribute
     * @param array $data
     * @return Attribute|false
     */
    public function createAttribute(array $data): Attribute|false;


    /**
     * create attribute
     * @param array $data
     * @return Attribute|false
     */
    public function createAttributeProduct(array $data): AttributeProduct|false;

    /**
     * update attribute
     * @param Attribute $attribute
     * @param array $data
     * @return Attribute|false
     */
    public function updateAttribute(Attribute $attribute, array $data): Attribute|false;

    /**
     * delete attribute
     * @param Attribute $attribute
     * @return bool
     */
    public function deleteAttribute(Attribute $attribute): bool;
}
