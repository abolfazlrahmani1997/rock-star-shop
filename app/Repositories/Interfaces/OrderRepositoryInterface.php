<?php

namespace App\Repositories\Interfaces;

use App\Models\Discount;
use App\Models\DiscountAttributes;
use App\Models\Interfaces\HasDiscountInterface;
use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface OrderRepositoryInterface
{
    /**
     * get one user
     * @param int|null $id
     * @param bool $with_relation
     * @return Order
     */
    public function getOrder(int $id = null, bool $with_relation = false): Order;

    /**
     * get all user
     * @param bool $with_relation
     * @return Collection
     */
    public function getAllOrders( bool $with_relation = false): Collection;

    /**
     * create DiscountAttributes
     * @param array $data
     * @return User|false
     */
    public function createOrder(array $data): Order|false;

    /**
     * update DiscountAttributes
     * @param Order $user
     * @param array $data
     * @return User|false
     */
    public function updateOrder(Order $order, array $data): Order|false;


}
