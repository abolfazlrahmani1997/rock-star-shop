<?php

namespace App\Repositories\Interfaces;

use App\Models\AttributeProduct;
use Illuminate\Database\Eloquent\Collection;

interface AttributeProductRepositoryInterface
{
    /**
     * get and find one attribute_product
     * @param int $od
     * @param bool $with_relation
     * @return AttributeProduct
     */
    public function getAttributeProduct(int $id, bool $with_relation): AttributeProduct;

    /**
     * get all attribute_product
     * @param bool $with_relation
     * @return Collection
     */
    public function getAllAttributeProduct(bool $with_relation): Collection;

    /**
     * create attribute_product
     * @param array $data
     * @return AttributeProduct|false
     */
    public function createAttributeProduct(array $data): AttributeProduct|false;

    /**
     * update attribute_product
     * @param AttributeProduct $attribute_product
     * @param array $data
     * @return AttributeProduct|false
     */
    public function updateAttributeProduct(AttributeProduct $attribute_product, array $data): AttributeProduct|false;

    /**
     * delete attribute_product
     * @param AttributeProduct $attribute_product
     * @return bool
     */
    public function deleteAttributeProduct(AttributeProduct $attribute_product): bool;
}
