<?php

namespace App\Helper;

use Illuminate\Http\UploadedFile;

class FileDirectoryHelper
{
    static function getDirectory(UploadedFile $file): string
    {
        $mime = $file->getMimeType();
        $result = in_array($mime, config('filesystems.image_support'));
        if (!$result) {
            return false;
        }
        return "image";
    }
}
