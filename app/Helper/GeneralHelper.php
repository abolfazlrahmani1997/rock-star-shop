<?php

namespace App\Helper;

class GeneralHelper
{
    /**
     * @param string $data
     * @param string $type
     * @return string
     */
    static function getRegular(string $data, string $type): string
    {
        return $type . $data . $type;
    }
}
