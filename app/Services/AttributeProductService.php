<?php

namespace App\Services;

use App\Helper\ResponseWrapper;
use App\Http\Resources\AttributeProductResource;
use App\Http\Resources\AttributeResource;
use App\Repositories\Interfaces\AttributeProductRepositoryInterface;
use App\Services\Interfaces\AttributeProductServiceInterface;
use Illuminate\Http\JsonResponse;

class AttributeProductService implements AttributeProductServiceInterface
{
    /**
     * Define Attribute
     * @var AttributeProductRepositoryInterface
     */
    private AttributeProductRepositoryInterface $attribute_product_repository;

    /**
     * Define Wrapper
     * @var ResponseWrapper
     */
    private ResponseWrapper $response_wrapper;

    /**
     * Inject Attribute
     * @param AttributeProductRepositoryInterface $attribute_product_repository
     */
    public function __construct(AttributeProductRepositoryInterface $attribute_product_repository, ResponseWrapper $response_wrapper)
    {
        $this->attribute_product_repository = $attribute_product_repository;
        $this->response_wrapper = $response_wrapper;
    }

    /**
     * GetSingle AttributeProduct
     * @param int $id
     * @return JsonResponse
     */
    public function getAttributeProduct(int $id): JsonResponse
    {
        $attribute_product = $this->attribute_product_repository->getAttributeProduct(id: $id, with_relation: false);
        return $this->response_wrapper
            ->setResource(AttributeProductResource::class)
            ->setData($attribute_product)
            ->setStatus(200)
            ->generateSingleResponse();
    }

    /**
     * get All AttributeProducts
     * @param array $data
     * @return mixed
     */
    public function getAllAttributeProducts(array $data = null): JsonResponse
    {
        $result = $this->attribute_product_repository->getAllAttributeProduct(with_relation: true);
        if (!$result) {
            return $this->response_wrapper
                ->setMessage('cant show attribute_product')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        return $this->response_wrapper
            ->setResource(AttributeProductResource::class)
            ->setData($result)
            ->setStatus(200)
            ->generateCollectionResponse();
    }

    /**
     * Create AttributeProduct
     */
    public function createAttributeProduct(array $data): JsonResponse
    {
        $result = $this->attribute_product_repository->createAttributeProduct(data: $data);
        if (!$result) {
            return $this->response_wrapper
                ->setMessage('cant create attribute_product')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        return $this->response_wrapper
            ->setResource(AttributeProductResource::class)
            ->setData($result)
            ->setMessage('created attribute_product')
            ->setStatus(200)
            ->generateSingleResponse();
    }

    /**
     * Update AttributeProduct
     * @return mixed
     */
    public function updateAttributeProduct(int $id, array $data): JsonResponse
    {
        $result = $this->attribute_product_repository->getAttributeProduct(id: $id, with_relation: false);
        $result = $this->attribute_product_repository->updateAttributeProduct(attribute_product: $result, data: $data);
        if (!$result) {
            return $this->response_wrapper
                ->setMessage('cant update attribute_product')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        return $this->response_wrapper
            ->setResource(AttributeProductResource::class)
            ->setData($result)
            ->setMessage('updated attribute_product')
            ->setStatus(200)
            ->generateSuccessResponse();
    }

    /**
     * Delete AttributeProduct
     * @param int $id
     * @return JsonResponse
     */
    public function deleteAttributeProduct(int $id): JsonResponse
    {
        $result = $this->attribute_product_repository->getAttributeProduct(id: $id, with_relation: false);
        $result = $this->attribute_product_repository->deleteAttributeProduct(attribute_product: $result);
        if (!$result) {
            return $this->response_wrapper
                ->generateFailedResponse(message: 'delete failed');
        }
        return $this->response_wrapper
            ->generateSuccessResponse(message: 'deleted success');
    }
}
