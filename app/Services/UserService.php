<?php

namespace App\Services;

use App\Helper\ResponseWrapper;
use App\Http\Resources\UserResource;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Services\Interfaces\UserServiceInterface;
use Illuminate\Http\JsonResponse;

class UserService implements UserServiceInterface
{
    /**
     * @var ResponseWrapper
     */
    private ResponseWrapper $response_wrapper;
    /**
     * inject User Repository
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $user_repository;

    /**
     * Inject Dependency For in service
     * @param UserRepositoryInterface $user_repository
     * @param ResponseWrapper $response_wrapper
     */
    public function __construct(UserRepositoryInterface $user_repository, ResponseWrapper $response_wrapper)
    {
        $this->response_wrapper = $response_wrapper;
        $this->user_repository = $user_repository;
    }

    /**
     * get & find one User
     * @param int $id
     * @return JsonResponse
     */
    public function getUser(int $id): JsonResponse
    {
        $result = $this->user_repository->getUser(id: $id, with_relation: true);
        return $this->response_wrapper
            ->setResource(UserResource::class)
            ->setData($result)
            ->setStatus(200)
            ->generateSingleResponse();
    }

    /**
     * get all User
     * @param array $data
     * @return JsonResponse
     */
    public function getAllUser(array $data): JsonResponse
    {
        $result = $this->user_repository->getAllUser(with_relation: true);
        if (!$result) {
            return $this->response_wrapper
                ->setMessage('cant show Users')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        return $this->response_wrapper
            ->setResource(UserResource::class)
            ->setData($result)
            ->setStatus(200)
            ->generateCollectionResponse();
    }

    /**
     * create User
     * @param array $data
     * @return JsonResponse
     */
    public function createUser(array $data): JsonResponse
    {
        $result = $this->user_repository->createUser(data: $data);
        if (!$result) {
            return $this->response_wrapper
                ->setMessage('cant create user')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        return $this->response_wrapper
            ->setResource(UserResource::class)
            ->setData($result)
            ->setMessage('created user')
            ->setStatus(200)
            ->generateSingleResponse();
    }

    /**
     * update User
     * @param int $id
     * @param array $data
     * @return JsonResponse
     */
    public function updateUser(int $id, array $data): JsonResponse
    {
        $user = $this->user_repository->getUser(id: $id);
        $result = $this->user_repository->updateUser(user: $user, data: $data);
        if (!$result) {
            return $this->response_wrapper
                ->setMessage('cant update user')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        return $this->response_wrapper
            ->setResource(UserResource::class)
            ->setData($result)
            ->setMessage('updated user')
            ->setStatus(200)
            ->generateSingleResponse();
    }
}
