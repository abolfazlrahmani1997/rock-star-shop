<?php

namespace App\Services\Interfaces;

use Illuminate\Http\JsonResponse;

interface AttributeServiceInterface
{
    /**
     * GetSingle Attribute
     * @param int $id
     * @return JsonResponse
     */
    public function getAttribute(int $id): JsonResponse;

    /**
     * get All Attributes
     * @param array $data
     * @return mixed
     */
    public function getAllAttributes(array $data = null): JsonResponse;

    /**
     * Create Attribute
     */
    public function createAttribute(array $data): JsonResponse;

    /**
     * Update Attribute
     * @return mixed
     */
    public function updateAttribute(int $id, array $data): JsonResponse;

    /**
     * Delete Attribute
     * @param int $id
     * @return JsonResponse
     */
    public function deleteAttribute(int $id): JsonResponse;
}
