<?php

namespace App\Services\Interfaces;

use Illuminate\Http\JsonResponse;

interface UserServiceInterface
{
    /**
     * get & find one User
     * @param int $id
     * @return JsonResponse
     */
    public function getUser(int $id): JsonResponse;

    /**
     * get all User
     * @param array $data
     * @return JsonResponse
     */
    public function getAllUser(array $data): JsonResponse;

    /**
     * create User
     * @param array $data
     * @return JsonResponse
     */
    public function createUser(array $data): JsonResponse;

    /**
     * update User
     * @param int $id
     * @param array $data
     * @return JsonResponse
     */
    public function updateUser(int $id, array $data): JsonResponse;

}
