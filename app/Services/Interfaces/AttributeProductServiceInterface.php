<?php

namespace App\Services\Interfaces;

use Illuminate\Http\JsonResponse;

interface AttributeProductServiceInterface
{
    /**
     * GetSingle AttributeProduct
     * @param int $id
     * @return JsonResponse
     */
    public function getAttributeProduct(int $id): JsonResponse;

    /**
     * get All AttributeProducts
     * @param array $data
     * @return mixed
     */
    public function getAllAttributeProducts(array $data = null): JsonResponse;

    /**
     * Create AttributeProduct
     */
    public function createAttributeProduct(array $data): JsonResponse;

    /**
     * Update AttributeProduct
     * @return mixed
     */
    public function updateAttributeProduct(int $id, array $data): JsonResponse;

    /**
     * Delete AttributeProduct
     * @param int $id
     * @return JsonResponse
     */
    public function deleteAttributeProduct(int $id): JsonResponse;
}
