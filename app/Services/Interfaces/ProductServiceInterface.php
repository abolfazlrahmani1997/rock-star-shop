<?php

namespace App\Services\Interfaces;

use Illuminate\Http\JsonResponse;

interface ProductServiceInterface
{
    /**
     * get & find one Product
     * @param int $id
     * @return JsonResponse
     */
    public function getProduct(int $id): JsonResponse;

    /**
     * get all Product
     * @param array $data
     * @return JsonResponse
     */
    public function getAllProduct(array $data = null): JsonResponse;

    /**
     * create Product
     * @param array $data
     * @return JsonResponse
     */
    public function createProduct(array $data): JsonResponse;

    /**
     * update Product
     * @param int $id
     * @param array $data
     * @return JsonResponse
     */
    public function updateProduct(int $id, array $data): JsonResponse;

    /**
     * delete Product
     * @param int $id
     * @return JsonResponse
     */
    public function deleteProduct(int $id): JsonResponse;

}
