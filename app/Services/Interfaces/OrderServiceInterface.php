<?php

namespace App\Services\Interfaces;

use Illuminate\Http\JsonResponse;

interface OrderServiceInterface
{

    /**
     * Get Order
     * @param int $id
     * @return JsonResponse
     */
    public function getOrder(int $id): JsonResponse;

    /**
     * Get All Order
     * @param array $data
     * @return JsonResponse
     */
    public function getAllOrder(array $data = null): JsonResponse;

    /**
     * Create Order
     * @param array $data
     * @return JsonResponse
     */
    public function createOrder(array $data): JsonResponse;


    public function addOrderItem(array $data): JsonResponse;

    /**
     * Update Order
     * @param array $data
     * @return JsonResponse
     */
    public function updateOrder(int $id, array $data): JsonResponse;

}
