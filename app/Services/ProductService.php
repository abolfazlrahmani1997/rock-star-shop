<?php

namespace App\Services;

use App\Events\AttributeEvent;
use App\Events\SpecificationEvent;
use App\Helper\ResponseWrapper;
use App\Http\Resources\ProductAllResource;
use App\Http\Resources\ProductResource;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use App\Services\Interfaces\ProductServiceInterface;
use App\Services\Interfaces\StorageServiceInterface;
use Illuminate\Http\JsonResponse;

class ProductService implements ProductServiceInterface
{
    /**
     * @var ResponseWrapper
     */
    private ResponseWrapper $response_wrapper;

    /**
     * define product repository
     * @var ProductRepositoryInterface
     */
    private ProductRepositoryInterface $product_repository;

    /**
     * define storage service
     * @var StorageServiceInterface
     */
    private StorageServiceInterface $storage_service;

    /**
     * Inject Dependency For in service
     * @param ProductRepositoryInterface $product_repository
     * @param ResponseWrapper $response_wrapper
     */
    public function __construct(ProductRepositoryInterface $product_repository, ResponseWrapper $response_wrapper, StorageServiceInterface $storage_service)
    {
        $this->product_repository = $product_repository;
        $this->response_wrapper = $response_wrapper;
        $this->storage_service = $storage_service;
    }

    /**
     * get & find one Product
     * @param int $id
     * @return JsonResponse
     */
    public function getProduct(int $id): JsonResponse
    {
        $result = $this->product_repository->getProduct(id: $id, with_relation: true);
        return $this->response_wrapper
            ->setResource(ProductResource::class)
            ->setData($result)
            ->setStatus(200)
            ->generateSingleResponse();
    }

    /**
     * get all Product
     * @param array $data
     * @return JsonResponse
     */
    public function getAllProduct(array $data = null): JsonResponse
    {
        $result = $this->product_repository->getAllProducts(data: $data, with_relation: true);
        if (!$result) {
            return $this->response_wrapper
                ->setMessage('cant show products')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        return $this->response_wrapper
            ->setResource(ProductAllResource::class)
            ->setData($result)
            ->setStatus(200)
            ->generateCollectionResponse();
    }

    /**
     * create Product
     * @param array $data
     * @return JsonResponse
     */
    public function createProduct(array $data): JsonResponse
    {

        $result = $this->product_repository->createProduct(data: $data);
        if (isset($data['attribute']))
        {
            event(new AttributeEvent(product:  $result,data: $data['attribute']));
        }
        if (isset($data['specification'])) {
            event(new SpecificationEvent(product: $result, data: $data['specification']));
        }
        if (!$result) {
            return $this->response_wrapper
                ->setMessage('cant create product')
                ->setStatus(404)
                ->generateFailedResponse();
        }

        return $this->response_wrapper
            ->setResource(resource:ProductResource::class)
            ->setData($result)
            ->setStatus(200)
            ->generateSingleResponse();
    }

    /**
     * update Product
     * @param int $id
     * @param array $data
     * @return JsonResponse
     */
    public function updateProduct(int $id, array $data): JsonResponse
    {
        $product = $this->product_repository->getProduct(id: $id);
        $result = $this->product_repository->updateProduct(product: $product, data: $data);
        if (!$result) {
            return $this->response_wrapper
                ->setMessage('cant update product')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        return $this->response_wrapper
            ->setResource(ProductResource::class)
            ->setData($result)
            ->setMessage('updated product')
            ->setStatus(200)
            ->generateSingleResponse();
    }

    /**
     * delete Product
     * @param int $id
     * @return JsonResponse
     */
    public function deleteProduct(int $id): JsonResponse
    {
        $product = $this->product_repository->getProduct(id: $id);
        $result = $this->product_repository->deleteProduct(product: $product);
        if (!$result) {
            return $this->response_wrapper
                ->generateFailedResponse(message: 'delete failed');
        }
        return $this->response_wrapper
            ->generateSuccessResponse(message: 'deleted success');
    }
}
