<?php

namespace App\Services;

use App\Events\SetTotalAmmont;
use App\Helper\ResponseWrapper;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Repositories\Interfaces\OrderProductRepositoryInterface;
use App\Repositories\Interfaces\OrderRepositoryInterface;
use App\Services\Interfaces\OrderServiceInterface;
use Illuminate\Http\JsonResponse;

class OrderService implements OrderServiceInterface
{
    /**
     * Define OrderRepository
     * @var OrderRepositoryInterface
     */
    private OrderRepositoryInterface $order_repository;

    /**
     * Response Wrapper
     * @var ResponseWrapper
     */
    private ResponseWrapper $response_wrapper;

    /**
     * @var OrderProductRepositoryInterface
     */
    private OrderProductRepositoryInterface $order_product_repository;

    /**
     * Order Repository
     * @param OrderRepositoryInterface $order_repository
     * @param ResponseWrapper $response_wrapper
     */
    public function __construct(OrderRepositoryInterface $order_repository, OrderProductRepositoryInterface $order_product_repository, ResponseWrapper $response_wrapper)
    {
        $this->order_repository = $order_repository;
        $this->response_wrapper = $response_wrapper;
        $this->order_product_repository = $order_product_repository;

    }

    /**
     * Get Order
     * @param int $id
     * @return JsonResponse
     */
    public function getOrder(int $id): JsonResponse
    {
        $order = $this->order_repository->getOrder(id: $id, with_relation: true);
        return $this->response_wrapper
            ->setResource(OrderResource::class)
            ->setData(data: $order)
            ->setStatus(200)
            ->generateSingleResponse();
    }

    /**
     * Get All Order
     * @param array $data
     * @return JsonResponse
     */
    public function getAllOrder(array $data = null): JsonResponse
    {
        $result = $this->order_repository->getAllOrders(with_relation: true);
        if (!$result) {
            return $this->response_wrapper
                ->setMessage('cant show Shops')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        return $this->response_wrapper
            ->setResource(OrderResource::class)
            ->setData(data: $result)
            ->setStatus(200)
            ->generateCollectionResponse();
    }

    /**
     * Create Order
     * @param array $data
     * @return JsonResponse
     */
    public function createOrder(array $data): JsonResponse
    {
        $order = $this->order_repository->createOrder(data: $data);
        if (!$order) {
            return $this->response_wrapper
                ->setMessage('cant create order')
                ->setStatus(404)
                ->generateFailedResponse();
        }
        return $this->response_wrapper
            ->setResource(OrderResource::class)
            ->setData(data: $order)
            ->setMessage('created order')
            ->setStatus(200)
            ->generateSingleResponse();
    }

    public function addOrderItem(array $data): JsonResponse
    {
        if (isset($data['order_id'])) {

            $order = $this->order_repository->getOrder(id: $data['order_id']);
        } else {

            $order = $this->order_repository->createOrder(data: $data);
        }

        if (!$order) {
            return $this->response_wrapper
                ->setMessage('cant create order')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        $data['order_id'] = $order->id;
        $order_item = $this->order_product_repository->createOrderProduct(data: $data);
        if (!$order_item) {
            return $this->response_wrapper
                ->setMessage('cant create orderItem')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        event(new SetTotalAmmont(order_id: $order->id, order_item_id: $order_item));
        return $this->response_wrapper
            ->setResource(OrderResource::class)
            ->setData(data: $order)
            ->setMessage('created order')
            ->setStatus(200)
            ->generateSingleResponse();
    }
    /**
     * Create OrderItem
     */


    /**
     * Update Order
     * @param array $data
     * @return JsonResponse
     */
    public function updateOrder(int $id, array $data): JsonResponse
    {
        $order = $this->order_repository->getOrder(id: $id);
        $order = $this->order_repository->updateOrder(order: $order, data: $data);
        if (!$order) {
            return $this->response_wrapper
                ->setMessage('cant update order')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        return $this->response_wrapper
            ->setResource(OrderResource::class)
            ->setData($order)
            ->setMessage('updated order')
            ->setStatus(200)
            ->generateSingleResponse();
    }
}
