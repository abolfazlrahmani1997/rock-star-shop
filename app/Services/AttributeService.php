<?php

namespace App\Services;

use App\Helper\ResponseWrapper;
use App\Http\Resources\AttributeProductResource;
use App\Http\Resources\AttributeResource;
use App\Repositories\Interfaces\AttributeRepositoryInterface;
use App\Services\Interfaces\AttributeServiceInterface;
use Illuminate\Http\JsonResponse;

class AttributeService implements AttributeServiceInterface
{

    /**
     * Define Attribute
     * @var AttributeRepositoryInterface
     */
    private AttributeRepositoryInterface $attribute_repository;

    /**
     * Define Wrapper
     * @var ResponseWrapper
     */
    private ResponseWrapper $response_wrapper;

    /**
     * Inject Attribute
     * @param AttributeRepositoryInterface $attribute_repository
     */
    public function __construct(AttributeRepositoryInterface $attribute_repository, ResponseWrapper $response_wrapper)
    {
        $this->attribute_repository = $attribute_repository;
        $this->response_wrapper = $response_wrapper;
    }

    /**
     * GetSingle Attribute
     * @param int $id
     * @return JsonResponse
     */
    public function getAttribute(int $id): JsonResponse
    {
        $attribute = $this->attribute_repository->getAttribute(id: $id, with_relation: false);
       return $this->response_wrapper
           ->setResource(AttributeResource::class)
           ->setData($attribute)
           ->setStatus(200)
           ->generateSingleResponse();
    }

    /**
     * get All Attributes
     * @param array $data
     * @return mixed
     */
    public function getAllAttributes(array $data = null): JsonResponse
    {
        $attributes = $this->attribute_repository->getAllAttribute(with_relation: false);
        if (!$attributes) {
            return $this->response_wrapper
                ->setMessage('cant show attributes')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        return $this->response_wrapper
            ->setResource(AttributeResource::class)
            ->setData($attributes)
            ->setStatus(200)
            ->generateCollectionResponse();
    }

    /**
     * Create Attribute
     */
    public function createAttribute(array $data): JsonResponse
    {
        $result = $this->attribute_repository->createAttribute(data: $data);
        if (!$result) {
            return $this->response_wrapper
                ->setMessage('cant create attribute')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        return $this->response_wrapper
            ->setResource(AttributeResource::class)
            ->setData($result)
            ->setMessage('created attribute')
            ->setStatus(200)
            ->generateSingleResponse();
    }

    /**
     * Update Attribute
     * @return mixed
     */
    public function updateAttribute(int $id, array $data): JsonResponse
    {
        $result = $this->attribute_repository->getAttribute(id: $id, with_relation: false);
        $result = $this->attribute_repository->updateAttribute(attribute: $result, data: $data);
        if (!$result) {
            return $this->response_wrapper
                ->setMessage('cant update attribute')
                ->setStatus(404)
                ->generateSingleResponse();
        }
        return $this->response_wrapper
            ->setResource(AttributeResource::class)
            ->setData($result)
            ->setMessage('updated attribute')
            ->setStatus(200)
            ->generateSuccessResponse();
    }

    /**
     * Delete Attribute
     * @param int $id
     * @return JsonResponse
     */
    public function deleteAttribute(int $id): JsonResponse
    {
        $result = $this->attribute_repository->getAttribute(id: $id, with_relation: false);
        $result = $this->attribute_repository->deleteAttribute(attribute: $result);
        if (!$result) {
            return $this->response_wrapper
                ->generateFailedResponse(message: 'delete failed');
        }
        return $this->response_wrapper
            ->generateSuccessResponse(message: 'deleted success');
    }
}
